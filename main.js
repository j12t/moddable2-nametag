/*
 * Scrollable name tag.
 * Based on examples from the Moddable SDK
 */

import {} from "piu/MC";
import CombTransition from "piu/CombTransition";

const nameStyle        = new Style({ font:"600 28px Open Sans", color: "white" });
const labelStyle       = new Style({ font:"400 20px Open Sans", color: "#c0c0ff" });
const backgroundSkin   = new Skin({ fill: "#2020b0" });

class Affiliation extends Container {
    constructor( image, width, height, url ) {
        super( null, {
            left: 0,
            right: 0,
            top: 0,
            bottom: 0,
            contents : [
                new Content( null, {
                    top: (130-height)/2,
                    width: width,
                    height: height,
                    skin: new Skin( {
                        texture: new Texture( image ),
                        x: 0,
                        y: 0,
                        width: width,
                        height: height
                    } )
                }),
                new Label( null, {
                    string: url,
                    style: labelStyle,
                    bottom: 10
                })
            ]
        } );
    }
}

let affiliations = [
    new Affiliation( "indiecomputing.com-100x100.png", 100, 100, "indiecomputing.com" ),
    new Affiliation( "me2balliance-195x92.png",        195,  92, "me2balliance.org" ),
    new Affiliation( "mydata-100x100.png",             100, 100, "MyDataSV.org" ),
    new Affiliation( "ubos-100x100.png",               100, 100, "ubos.net" ),
    new Affiliation( "project-springtime-112x63.png",  112,  63, "project-springtime.org" ),
    new Affiliation( "paradux-100x100.png",            100, 100, "github.com/paradux" )
];

class AffiliationScrollBehavior extends Behavior {
    onTouchBegan(content, id, x, y, ticks) {
        let oldAffIndex = affIndex;
        if( x < content.bounds.width/2 ) {
            affIndex -= 1;
            if( affIndex < 0 ) {
                affIndex = affiliations.length - 1;
            }
        } else {
            affIndex += 1;
            if( affIndex >= affiliations.length ) {
                affIndex = 0;
            }
        }
        let transition = new CombTransition(200, Math.quadEaseOut, "horizontal", 10 );
        aff.run(transition, affiliations[oldAffIndex], affiliations[affIndex] );
    }
}

let NametagApplication = Application.template( $ => ({
    skin: backgroundSkin,
    contents: [
        Column($, { // two elements: the name area, and the affiliation area
            top: 0,
            bottom: 0,
            left: 0,
            right: 0,
            contents : [
                Column($, { // two elements: first and last name
                    top: 40,
                    height: 120,
                    left: 0,
                    right: 0,
                    contents : [
                        Label($, {
                            top: 0,
                            height: 40,
                            left: 0,
                            right: 10, 
                            style: nameStyle,
                            string: "Johannes"
                        } ),
                        Label($, {
                            top: 0,
                            height: 40,
                            left: 0,
                            right: 0, 
                            style: nameStyle,
                            string: "Ernst"
                        } )
                    ]
                })
            ]
        })
    ]
}));


let ret = new NametagApplication(null, { commandListLength:4096, displayListLength:4096 });
let aff = new Container( null, {
    top: 0, bottom: 0, left: 0, right: 0,
    active: true,
    skin: new Skin({ fill: "#3030c0" }),
    Behavior: AffiliationScrollBehavior
});
let affIndex = 0;

aff.add( affiliations[affIndex] );
ret.first.add( aff );

application.rotation = 180;

export default ret;
